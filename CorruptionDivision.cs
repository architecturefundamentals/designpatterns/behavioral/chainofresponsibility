using FactoryMethod.ChainOfResponsibility.senteceFactory;

namespace FactoryMethod.ChainOfResponsibility
{
    public class CorruptionDivision : BaseCrimeHandler
    {
        private readonly int minNumberOfDays = 365;
        public CorruptionDivision(ISentenceCreator sentenceFactory){
            this.sentenceFactory = sentenceFactory;
        }
        public override ISentence applySentence(Crime crime)
        {
            if(crime.Type == CrimeType.Corruption){
                var sentence = this.sentenceFactory!.create();
                sentence.Punishment = "Prison";
                sentence.Days = (int)crime.SeverityType * minNumberOfDays;
                return sentence;
            }else{
                return base.applySentence(crime);
            }
        }
    }
}