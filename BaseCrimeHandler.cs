using FactoryMethod.ChainOfResponsibility.senteceFactory;
namespace FactoryMethod.ChainOfResponsibility
{
    public abstract class BaseCrimeHandler: ICrimeHandler
    {
        protected ICrimeHandler? next;
        protected ISentenceCreator? sentenceFactory;
        public void setNextHandler(ICrimeHandler next){
            this.next = next;
        }
        public virtual ISentence applySentence(Crime crime)
        {
            if(this.next != null){
                return this.next.applySentence(crime);
            }
            return this.sentenceFactory!.create();
        }
    }
}