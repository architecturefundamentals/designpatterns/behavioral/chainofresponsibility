namespace FactoryMethod.ChainOfResponsibility
{
    public enum CrimeType
    {
        Corruption,
        Child,
        CiberCrime,
        Murder,
        Steal
    }
}