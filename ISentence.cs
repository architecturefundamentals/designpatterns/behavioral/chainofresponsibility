using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.ChainOfResponsibility
{
    public interface ISentence
    {
        public String Punishment{
            get;
            set;
        }
        public int Days{
            get;
            set;
        }
    }
}