using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FactoryMethod.ChainOfResponsibility.senteceFactory;

namespace FactoryMethod.ChainOfResponsibility.factory
{
    public class CiberCrimeDivisionFactory : ICrimeHandlerCreator
    {
        public ICrimeHandler create()
        {
            ISentenceCreator senteceFactory = new SentenceFactory();
            return new CiberCrimeDivision(senteceFactory);
        }
    }
}