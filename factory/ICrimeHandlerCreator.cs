namespace FactoryMethod.ChainOfResponsibility.factory
{
    public interface ICrimeHandlerCreator
    {
        public ICrimeHandler create();
    }
}