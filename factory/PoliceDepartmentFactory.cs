namespace FactoryMethod.ChainOfResponsibility.factory
{
    public class PoliceDepartmentFactory : ICrimeHandlerCreator
    {
        public ICrimeHandler create()
        {
            var childHandler = (new ChildDivisionFactory()).create();
            var ciberCrimeHandler = (new CiberCrimeDivisionFactory()).create();
            var corruptionHandler = (new CorruptionDivisionFactory()).create();
            var murderHandler = (new MurderDivisionFactory()).create();
            childHandler.setNextHandler(ciberCrimeHandler);
            ciberCrimeHandler.setNextHandler(corruptionHandler);
            corruptionHandler.setNextHandler(murderHandler);
            return childHandler;
        }
    }
}