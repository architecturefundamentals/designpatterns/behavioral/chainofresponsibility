using FactoryMethod.ChainOfResponsibility.senteceFactory;

namespace FactoryMethod.ChainOfResponsibility
{
    public class CiberCrimeDivision : BaseCrimeHandler
    {
        private readonly int minNumberOfDays = 181;
        public CiberCrimeDivision(ISentenceCreator sentenceFactory){
            this.sentenceFactory = sentenceFactory;
        }
        public override ISentence applySentence(Crime crime)
        {
            if(crime.Type == CrimeType.CiberCrime){
                var sentence = this.sentenceFactory!.create(); 
                sentence.Punishment = "Prison";
                sentence.Days = (int)crime.SeverityType * minNumberOfDays;
                return sentence;
            }else{
                return base.applySentence(crime);
            }
        }
    }
}