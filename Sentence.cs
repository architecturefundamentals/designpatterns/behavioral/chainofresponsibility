namespace FactoryMethod.ChainOfResponsibility
{
    public class Sentence: ISentence
    {
        public String Punishment{
            get;
            set;
        }
        public int Days{
            get;
            set;
        }
        public Sentence(){
            this.Punishment = "no sentence";
            this.Days = 0;
        }
    }
}