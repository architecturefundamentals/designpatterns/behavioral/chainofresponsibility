# CHAIN OF RESPONSIBILITY PATTERN

---
## A bit of context ...
>### _What is this pattern about?_
>This pattern it's a structural pattern, it ets you pass requests along a chain of handlers. Upon receiving a request, each handler decides either to process the request or to pass it to the next handler in the chain.
>
> It's usually used in the next cases:
>  * When your program is expected to process different kinds of requests in various ways, but the exact types of requests and their sequences are unknown beforehand.
>  * When it’s essential to execute several handlers in a particular order.
>  * When the set of handlers and their order are supposed to change at runtime.
> 
---
## But this simple example is about...

![Demo](img/Adapter Pattern-ChainOfResposibility.jpg)

and Done!
:smile:
---
_Tech: C#_ 