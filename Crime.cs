namespace FactoryMethod.ChainOfResponsibility
{
    public class Crime
    {
        public CrimeType Type{
            get;
            set;
        }

        public CrimeSeverity SeverityType{
            get;
            set;
        }

        public Crime(CrimeType type, CrimeSeverity SeverityType){
            this.Type = type;
            this.SeverityType = SeverityType;
        }
    }
}