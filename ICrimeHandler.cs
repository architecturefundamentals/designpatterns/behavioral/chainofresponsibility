namespace FactoryMethod.ChainOfResponsibility
{
    public interface ICrimeHandler
    {
        public void setNextHandler(ICrimeHandler next);
        public ISentence applySentence(Crime crime);
    }
}