using FactoryMethod.ChainOfResponsibility.senteceFactory;
namespace FactoryMethod.ChainOfResponsibility
{
    public class MurderDivision : BaseCrimeHandler
    {
        private readonly int minNumberOfDays = 3000;
        public MurderDivision(ISentenceCreator sentenceFactory){
            this.sentenceFactory = sentenceFactory;
        }
        public override ISentence applySentence(Crime crime)
        {
            if(crime.Type == CrimeType.Murder){
                var sentence = this.sentenceFactory!.create();
                sentence.Punishment = "Prison";
                sentence.Days = (int)crime.SeverityType * minNumberOfDays;
                return sentence;
            }else{
                return base.applySentence(crime);
            }
        }
    }
}