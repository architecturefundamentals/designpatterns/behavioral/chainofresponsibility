namespace FactoryMethod.ChainOfResponsibility
{
    public enum CrimeSeverity
    {
        Critical = 5,
        Major = 3,
        Minor = 1
    }
}