﻿using FactoryMethod.ChainOfResponsibility;
using FactoryMethod.ChainOfResponsibility.factory;

ICrimeHandler policeDepartment = (new PoliceDepartmentFactory()).create();

Crime childCrime = new Crime(CrimeType.Child, CrimeSeverity.Minor);
Crime ciberCrime = new Crime(CrimeType.CiberCrime, CrimeSeverity.Major);
Crime corruptionCrime = new Crime(CrimeType.Corruption, CrimeSeverity.Minor);
Crime murderCrime = new Crime(CrimeType.Steal, CrimeSeverity.Critical);

var childSentence = policeDepartment.applySentence(childCrime);
Console.WriteLine("Punishment: " + childSentence.Punishment + "       days: " + childSentence.Days);

var ciberSentence = policeDepartment.applySentence(ciberCrime);
Console.WriteLine("Punishment: " + ciberSentence.Punishment + "        days: " + ciberSentence.Days);

var curruptionSentence = policeDepartment.applySentence(corruptionCrime);
Console.WriteLine("Punishment: " + curruptionSentence.Punishment + "        days: " + curruptionSentence.Days);

var murderSentence = policeDepartment.applySentence(murderCrime);
Console.WriteLine("Punishment: " + murderSentence.Punishment + "        days: " + murderSentence.Days);

