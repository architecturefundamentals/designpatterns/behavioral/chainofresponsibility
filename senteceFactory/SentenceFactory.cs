using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FactoryMethod.ChainOfResponsibility.senteceFactory
{
    public class SentenceFactory : ISentenceCreator
    {
        public ISentence create()
        {
            return new Sentence();
        }
    }
}