using FactoryMethod.ChainOfResponsibility.senteceFactory;
namespace FactoryMethod.ChainOfResponsibility
{
    public class ChildDivision : BaseCrimeHandler
    {
        
        private readonly int minNumberOfDays = 90;
        public ChildDivision(ISentenceCreator sentenceFactory){
            this.sentenceFactory = sentenceFactory;
        }
        public override ISentence applySentence(Crime crime)
        {
            if(crime.Type == CrimeType.Child){
                var sentence = this.sentenceFactory!.create(); 
                sentence.Punishment = "Reformatory";
                sentence.Days = (int)crime.SeverityType * minNumberOfDays;
                return sentence;
            }else{
                return base.applySentence(crime);
            }
        }
    }
}